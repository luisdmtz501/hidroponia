<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

/**
 * 
 */
class ScheduleModel
{
	private $db;
	private $response;
	private $tableSchedule = 'horario';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	public function listSchedule($idEstablecimiento){
		$data = $this->db->from($this->tableSchedule)
							->where('idEstablecimiento', $idEstablecimiento)
							->orderBy('idEstablecimiento DESC')
    					    ->fetchAll();

    		   $this->response->result = ['Data' => $data];
    	return $this->response->SetResponse(true);
		
	}
	
	public function add($data){
		$tam = count($data);
		for ($i=0; $i < $tam; $i++) { 
			$add = $this->db->insertInto($this->tableSchedule, $data[$i])
						    ->execute();
		}		
			   $this->response->result = "Horarios gurdados de manera exitosa";
		return $this->response->SetResponse(true);
	}

	public function update($data,$idEstablecimiento){
		$eliminar = $this->db->deleteFrom($this->tableSchedule)
							 ->where('idEstablecimiento',$idEstablecimiento)
							  ->execute();
							  
		$tam = count($data);
		for ($i=0; $i < $tam; $i++) { 
			$add = $this->db->insertInto($this->tableSchedule, $data[$i])
							->execute();
		}	
			   $this->response->result = "Horarios actualizados de manera exitosa";
		return $this->response->SetResponse(true);
	}

}
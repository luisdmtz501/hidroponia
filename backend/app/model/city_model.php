<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;


class CityModel
{
	private $db;
	private $response;
	private $table = 'ciudad';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#servicios
	public function toList(){
		$data = $this->db->from($this->table)
						 ->where("status = 1")
						 ->where("idCiudad !=1")
						 ->fetchAll();
			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}
	
}
 ?>
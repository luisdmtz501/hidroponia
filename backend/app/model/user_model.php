<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class UserModel 
{
	private $db;
	private $response;
	private $tablePerson = 'persona';
	private $dirSubida = '/var/www/html/lubo_back_dev/img/'; #local
    private $dirServer = 'http://localhost:8080/lubo_back_dev/img/'; #local
	
	public function __CONSTRUCT($db){
		$this->db = $db;
        $this->response = new Response();
	}

	#-----------------------------------------SERVICIOS----------------------------------------------------------------
	#servicio de Registro
	#Valida que no halla algun otro usuario con la misma informacion 	
	public function registerUser($data){
		if(filter_var($data['email'],FILTER_VALIDATE_EMAIL)) {
			if(strlen($data['apellidos']) >= 2){
				#data(contiene el body que son los datos que se van a insertar en la tabla)
				$email = $this->db->from($this->tablePerson)
								->where('email', $data['email'])
								->where('idTipoUsuario', $data['idTipoUsuario'])
								->where('idStatusPersona != 2')
								->fetch(); #fetch cuando solo es uno solo

				if($email!=false){
						$this->response->errors = "Ya existe un usuario con esa información";
					return $this->response->SetResponse(false);
				}else{
					$array = array('password' => Cifrado::Sha512($data['password'])); #para encriptar la contraseña
					$new_data = array_merge($data, $array); #array_merge:vincula n numero de arreglos para hacerlo uno solo

					$register = $this->db->insertInto($this->tablePerson, $new_data)
										->execute(); #excute(ejecuta la consulta)

						$this->response->result = $register;
					return $this->response->SetResponse(true, "Registro exitoso");
					#falta mostrar la fecha actual del registro
				}
			}else{
					   $this->response->errors='Apellido no valido.';
				return $this->response->SetResponse(false);
			}
		}else{
				   $this->response->errors='Correo no valido.';
	        return $this->response->SetResponse(false); 
		}
	}

	public function informationUser($id){
		$obtener = $this->db->from($this->tablePerson)
							->select(null)
						    ->select('nombre, apellidos, email, password, urlFoto, telefono,sexo,idCiudad')
						    ->where('idUsuario', $id)
						    ->fetch();

				   $this->response->result = $obtener;	
			return $this->response->SetResponse(true);
	}

	public function updateInformationUser($data,$idUsuario){
	    $buscar = $this->db->from($this->tablePerson)
	                     ->where('idUsuario', $idUsuario)
	                     ->fetch();

	    if ($buscar != true) {
	             $this->response->errors='Usuario no existe.';
	      return $this->response->SetResponse(false);
	    }else{
			if (isset($data['password'])) {
				$data['password'] = Cifrado::Sha512($data['password']);
			}else{
				unset($data['password']);
			}
	    	 $actualizar = $this->db->update($this->tablePerson, $data) 
	                                ->where('idUsuario',$idUsuario)          
	                                ->execute();

	        if ($actualizar==true) {
	                 $this->response->result=$actualizar;
	          return $this->response->SetResponse(true,'Actualización correcta.');
	        }else{
	                 $this->response->errors='No se pudo actualizar.';
	          return $this->response->SetResponse(false); 
	        }
	    }
    }

	public function deleteUser($id){
		$this->db->update($this->tablePerson)
				 ->set('idStatusPersona', 2) #set actualiza la columna indicada por el valor indicado
				 ->where('idUsuario', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
	}

	public function updateEmailUser($data,$id){
	    $buscar = $this->db->from($this->tablePerson)
	                       ->where('email', $data['email'])
	                       ->fetch();

	    if ($buscar != false) {
	             $this->response->errors='Correo ligado a otra una cuenta';
	      return $this->response->SetResponse(false);
	    }else{
	        $actualizar=$this->db->update($this->tablePerson, $data)
	                             ->where('idUsuario',$id)        
	                             ->execute();
	          
	               $this->response->result=$actualizar;
	        return $this->response->SetResponse(true,'Actualización correcta.');
	    }
    }
 
    public function toList($idTipoUsuario){
		if ($idTipoUsuario == 3) {
			$data = $this->db->from($this->tablePerson)
    					 ->select(null)
						 ->select('idUsuario, persona.nombre, apellidos, persona.email, password, sexo, persona.urlFoto, persona.telefono, idTipoUsuario, idstatusPersona, IFNULL(establecimiento.nombre, "Sin detalle") AS nombreEstablecimiento')
						 ->leftJoin('establecimiento on establecimiento.idAdministrador = persona.idUsuario')
						 ->where('idTipoUsuario',$idTipoUsuario)
						 ->where('idstatusPersona != 2')
						 ->orderBy('idUsuario DESC') #ASC
    					 ->fetchAll();
		} else {
			$data = $this->db->from($this->tablePerson)
    					 ->select(null)
    					 ->select('idUsuario, nombre, apellidos, email, password, sexo, urlFoto, telefono, idTipoUsuario, idstatusPersona,FechaRegistro')
						 ->where('idTipoUsuario',$idTipoUsuario)
						 ->where('idstatusPersona != 2')
						 ->orderBy('idUsuario DESC') #ASC
    					 ->fetchAll();
		}

    	$total = $this->db->from($this->tablePerson)
						  ->select('COUNT(*) total')
						  ->where('idTipoUsuario',$idTipoUsuario)
						  ->where('idstatusPersona != 2')
    					  ->fetch()
    					  ->total;

    			$this->response->result = ['Data' => $data, 'Total' => $total];
    	return $this->response->SetResponse(true);
    }
 
    #PENDIENTE---------------------------
    #Mostrar foto de perfil
    public function obtainPhotoUser($id){
    	$obtener = $this->db->from($this->tablePerson)
    						->select(null)
    						->select("urlFoto")
                        	->where('idUsuario', $id)
	                        ->limit(1)
	                        ->fetch();

	    if ($obtener != false) {
	             $this->response->result=$obtener;
	      return $this->response->SetResponse(true);
	    }else{
	             $this->response->errors='Error al cargar imagen.';
	      return $this->response->SetResponse(false);
	    }
    }

    #Actualizar contraseña 
    public function updatePassword($idUser, $password){
    	$buscar_usuario_registrado = $this->db->from($this->tablePerson)
    		->select('COUNT(*) AS Total')
	        ->where('idUsuario', $idUser)
	        ->fetch()
	        ->Total;

	    if ($buscar_usuario_registrado == 1) {
	    	$password = Cifrado::Sha512($password);

	    	$actualizar = $this->db->update($this->tablePerson)
	    		->set('password', $password)
				->where('idUsuario', $idUser)
				->execute();

	               $this->response->result = $actualizar;
	        return $this->response->SetResponse(true, 'Actualización correcta.');
	    } else {
	    		   $this->response->errors = "El usuario al que hace referencia no existe.";
	    	return $this->response->SetResponse(false);
	    }
    }

    

    #Recuperar contraseña
    public function recoverPassword(){
    	
    }		
  }
  
 ?>
<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado,
    App\Lib\Codigos,
    App\Lib\SMS,
    App\Lib\RestApi,
    App\Lib\RestApiMethod,
    App\Lib\Token,
    App\Lib\EmailRecoverPassword;

class AuthModel{
    private $db;
    private $tbUser = 'persona';
    private $tbEstablishment = 'establecimiento';
    private $tbTokenFirebase = 'tokenPush';
    private $tbsms = 'smsvalidacion';
    private $tbRecuperarPassword = 'codigoRecuperarPassword';
    private $response;

    public function __CONSTRUCT($db){
        $this->db = $db;
        $this->response = new Response();
    }

    public function logout($id){
        $actualizarActivo = $this->db->update($this->tbUser, ["activo"=>0])
                                     ->where('idUsuario', $id)
                                     ->execute();

        // PATCH
        $usuario = $this->db->from($this->tbUser)
                            ->select('tipousuario.Descripcion')
                            ->where('idUsuario',$id)
                            ->leftJoin('tipousuario on persona.idTipoUsuario = tipousuario.idTipoUsuario')
                            ->fetch();
        RestApi::call(
            RestApiMethod::PATCH,
            "$usuario->Descripcion/$usuario->idCiudad/$usuario->idUsuario.json",
            [
                'activo'    => 0
            ]
        );
                $this->response->result = "se cerro la sesion";
        return $this->response->SetResponse(true);
    }

    public function login($user, $password, $tipoUser,$plataforma,$tokenFB)
    {   
        $password = Cifrado::Sha512($password);

        $usuario = $this->db->from($this->tbUser)
                            ->select('tipousuario.Descripcion')
                            ->where('
                                (idStatusPersona <= 1) and 
                                (persona.idTipoUsuario = :tipousuario and password = :password) and
                                (telefono = :telefono or email = :email )',
                                array(
                                    ':tipousuario' => $tipoUser, 
                                    ':password' => $password, 
                                    ':telefono' => $user, 
                                    ':email' => $user
                                ))
                            ->leftJoin('tipousuario on persona.idTipoUsuario = tipousuario.idTipoUsuario')
                            ->fetch();

        if(is_object($usuario)){
             //actualizar activo en mysql
             $actualizarActivo = $this->db->update($this->tbUser, ["activo"=>1])
                                          ->where('idUsuario', $usuario->idUsuario)
                                          ->execute();
            // crea el nodo de Firebase
            RestApi::call(
                RestApiMethod::POST,
                "$usuario->Descripcion/$usuario->idCiudad/$usuario->idUsuario.json",
                []);
            //actualiza el nodo con la informacion del usuario 
            RestApi::call(
                RestApiMethod::PUT,
                "$usuario->Descripcion/$usuario->idCiudad/$usuario->idUsuario.json",
                [
                    'idUsuario' => $usuario->idUsuario,
                    'nombre'    => $usuario->nombre,
                    'apellidos' => $usuario->apellidos,
                    'email'     => $usuario->email,
                    'sexo'      => $usuario->sexo,
                    'urlPhoto'  => $usuario->urlFoto,
                    'telefono'  => $usuario->telefono,
                    'latitud'   => 0,
                    'longitud'  => 0,
                    'activo'    => 1
                ]
            );
            
            $mytoken = new Token();
            $token = $mytoken->addToken([
                'idUsuario' => $usuario->idUsuario,
                'nombre' => $usuario->nombre,
                'apellidos' => $usuario->apellidos,
                'email' => $usuario->email,
                'sexo' => $usuario->sexo,
                'urlPhoto'=>$usuario->urlFoto,
                'telefono' => $usuario->telefono,
            ]);
            // token firebase
            if($tokenFB != null){
                $data = [
                    'idUsuario' => $usuario->idUsuario,
                    'token' => $tokenFB,
                    'plataforma' => $plataforma
                ];

                $idUsuario = $data['idUsuario'];
                $idToken = $this->db->from($this->tbTokenFirebase)
                                    ->select('COUNT(*) Total')
                                    ->where('idUsuario', $idUsuario)
                                    ->fetch()
                                    ->Total;

                if ($idToken > 0) {
                    $actualizatoken = $this->db->update($this->tbTokenFirebase, $data)
                                               ->where('idUsuario', $idUsuario)
                                               ->execute();
                }else{
                    $altatoken=$this->db->insertInto($this->tbTokenFirebase, $data)
                                        ->execute();
                }
            }
            // fin token firebase
            $this->response->result = [
                                'token' => $token,
                                'id' => $usuario->idUsuario];
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors = "Credenciales no válidas";
            return $this->response->SetResponse(false);
        }
    }

    public function getData($data){
        $token = new Token();
        $res = $token->getData($data);
        if ($res === null) {
                   $this->response->errors = "Token incorrecto"; 
            return $this->response->SetResponse(false);
        }
               $this->response->result = $res;
        return $this->response->SetResponse(true);
    }
    #MÓVIL
    #Validar número de teléfono.
    public function validateNumber($codigoPais, $telefono, $tipoUsuario){
        $busca_numero_registrado = $this->db->from($this->tbUser)
            ->select('COUNT(*) as Num')
            ->where('
                idTipoUsuario = :tipo AND 
                codigoPais = :codigoPais AND
                Telefono = :tel AND 
                (idStatusPersona <= 1)', 
                array(
                    ':tipo' => $tipoUsuario, 
                    ':tel' => $telefono, 
                    ':codigoPais' => $codigoPais
                ))
            ->fetch()
            ->Num;
    
        if ($busca_numero_registrado == 0) {
            $codigo_verificacion = Codigos::Codigo(4);

            $cuerpo = "Tu código LuboGo es: $codigo_verificacion";
            $send = SMS::SendMensaje($codigoPais, $telefono, $cuerpo);
            // $send = '1';
            if ($send == '1') {
                $fecha_envio = date('Y-m-d H:i:s');
                $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+1 Hours")));

                $data_mensaje = [
                    "fechaAlta" => $fecha_envio,
                    "fechaExpiracion" => $fecha_expiracion,
                    "codigo" => $codigo_verificacion,
                    "codigoPais" => $codigoPais,
                    "telefono" => $telefono
                ];

                $altaCodigo = $this->db->insertInto($this->tbsms, $data_mensaje)
                                       ->execute();

                        $this->response->result = $altaCodigo;
                return  $this->response->SetResponse(true, 'Código de verificación enviado al teléfono: '.$telefono);
            } else {
                        $this->response->errors='Error al enviar el código de verificación al télefono: '.$telefono.' inténtelo de nuevo.';
                return  $this->response->SetResponse(false);
            }
        } else {
                    $this->response->errors='El número de télefono ya está registrado en otra cuenta.';
            return $this->response->SetResponse(false);
        }
    }

    #Validar código
    public function validateCode($codigoPais, $telefono, $codigo){
        $data_mensaje = $this->db->from($this->tbsms)
            ->select(null)
            ->select('idSmsValidacion AS idMensaje, codigo')
            ->where('
                idSmsValidacion = (SELECT MAX(idSmsValidacion) FROM smsvalidacion 
                WHERE telefono = :telefono AND 
                codigoPais = :codigoPais AND 
                status = 1)',
                array(
                    ':telefono' => $telefono,
                    ':codigoPais'=> $codigoPais
                ))
            ->fetch();
    
        if ($data_mensaje != false) {
            if ($data_mensaje->codigo === $codigo ){
                $updateStaus = $this->db->update($this->tbsms)
                    ->set('status', 0)
                    ->where('idSmsValidacion', $data_mensaje->idMensaje)
                    ->execute();

                $tokenRegistro = Token::LogReg(["tokenRegitro"=>$codigo]);

                       $this->response->result = $tokenRegistro;
                return $this->response->SetResponse(true, 'Código de verificación correcto.');
            }else{
                       $this->response->errors='El código que ingresó es incorrecto.';
                return $this->response->SetResponse(false); 
            }
        }else{
                   $this->response->errors='No hay código para este número teléfono: '.$telefono;
            return $this->response->SetResponse(false);
        }
    }

    #WEB
    #Enviar el código al correo
    public function sendCodeToEmail($email, $tipeUser){
        $busca_correo_registrado = $this->db->from($this->tbUser)
            ->select(null)
            ->select('CONCAT(nombre, " ", apellidos) AS NombreCompleto,idUsuario')
            ->where('
                idTipoUsuario = :tipo AND 
                email = :email AND 
                (idStatusPersona <= 1)',
                array(
                    ':tipo' => $tipeUser,
                    ':email' => $email
                ))
            ->fetch();

        if ($busca_correo_registrado != false) {
            $codigo_verificacion = Codigos::Codigo(6);
            
            #Mandar al correo electrónico
            $send_email = EmailRecoverPassword::sendEmail($email, $codigo_verificacion, $busca_correo_registrado->idUsuario, $busca_correo_registrado->NombreCompleto);

            // $send_email = 1;

            if ($send_email == 1) {
                $fecha_envio = date('Y-m-d H:i:s');
                $fecha_expiracion = date('Y-m-d H:i:s', (strtotime ("+24 Hours")));

                $data_mensaje = [
                    'Codigo' => $codigo_verificacion,
                    'FechaAlta' => $fecha_envio,
                    'FechaExpiracion' => $fecha_expiracion,
                    'Status' => 1,
                    'idUsuario' => $busca_correo_registrado->idUsuario
                ];

                $altaCodigo = $this->db->insertInto($this->tbRecuperarPassword, $data_mensaje)
                  ->execute();

                       $this->response->result = $busca_correo_registrado->NombreCompleto;
                return $this->response->SetResponse(true, 'Se ha enviado un código de verifiación al correo: '.$email);
            } else {
                       $this->response->errors = "No se ha podido enviar el correo electrónico.";
                return $this->response->SetResponse(false);
            }
        } else {
                   $this->response->errors = "El correo proporcionado no está asociado a una cuenta.";
            return $this->response->SetResponse(false);
        }
    }

    #Validar el código
    public function validateCodeEmail($idUser, $code){
        $data_mensaje = $this->db->from($this->tbRecuperarPassword)
            ->select(null)
            ->select('idCodigoRecuperarPassword AS idRecover, Codigo')
            ->where('
                idCodigoRecuperarPassword = (SELECT MAX(idCodigoRecuperarPassword) FROM codigoRecuperarPassword 
                WHERE idUsuario = :idUser AND
                status = 1)',
                array(
                    ':idUser' => $idUser
                ))
            ->fetch();

        if ($data_mensaje != false) {
            if ($data_mensaje->Codigo === $code) {
                $updateStaus = $this->db->update($this->tbRecuperarPassword)
                    ->set('Status', 0)
                    ->where('idCodigoRecuperarPassword', $data_mensaje->idRecover)
                    ->execute();

                $token_recover_password = Token::logRecoverPassword(["tokenRegistro" => $code]);

                       $this->response->result = $token_recover_password;
                return $this->response->SetResponse(true, 'Código de verificación correcto.');
            } else {
                       $this->response->errors='El código que ingresó es incorrecto.';
                return $this->response->SetResponse(false); 
            }
        } else {
                   $this->response->errors='No hay código para este email.';
            return $this->response->SetResponse(false);
        }
    }
}
<?php
namespace App\Model;

use App\Lib\Response;

class DashboardModel
{
    private $db;
    private $tbDashboard = 'dashboard';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

public function reportDashboard($de,$a){
        $this->response->result= (
            [
                "TotalViajes"=>150,
                "TotalGanancias"=>3000,
                "NumRepartidores"=>15,
                "NumUsuarios"=>20
            ]
            );

        return $this->response->SetResponse(true, "Reportes semanales");
    }

}
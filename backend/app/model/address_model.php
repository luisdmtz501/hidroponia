<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado;

class AddressModel 
{
	private $db;
	private $response;
	private $tableDirection = 'direccion';		
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#Servicios
	#Servicio para agregar dirección tanto del usuario como del estableDirectioncimiento
	public function add($data){
		$addAddress = $this->db->insertInto($this->tableDirection, $data)
						   ->execute();
		
		if ($addAddress != false) {
				   $this->response->result = $addAddress;
			return $this->response->SetResponse(true);
		}
	}


	public function toList($id,$tipo){
		$data = $this->db->from($this->tableDirection)
						 ->select(null)
						 ->select("
						   direccion.idDireccion, Calle, numeroExterior, numeroInterior, Colonia, Municipio, Ciudad, Latitud, Longitud, Estado, CodPostal, Guardado, idEstablecimiento, idUsuario, status, TextDir,false as Defecto,IFNULL(Descripcion,'') AS Descripcion")
						 ->where("$tipo",$id)
						 ->where('status = 1')
						 ->where('Guardado = 1')
						 ->orderBy('idDireccion DESC')
						 ->fetchAll();
		if(count($data)>0){
			if($tipo = "idUsuario"){
				// ultima usada
				$lastAdrees = $this->db->from($this->tableDirection)
									->select(null)
									->select("idDireccion")
									->leftJoin("orden on orden.idDireccionDestino = direccion.idDireccion")
									->where("direccion.idUsuario = $id
									AND direccion.Guardado = 1
									AND direccion.status = 1
									AND orden.idStatusOrden = 6")
									->orderBy('idOrden DESC')
									->limit(1)
									->fetch()
									->idDireccion;

				if($lastAdrees){
					foreach ($data AS $elemento){
						if($elemento->idDireccion == $lastAdrees){
							$elemento->Defecto = "1";
						}
					}
				}else{
					$data[0]->Defecto = "1";
				}
			}
				$this->response->result = $data;
			return $this->response->SetResponse(true);
		}else{
					$this->response->errors = "no hay direcciones registradas";
			return $this->response->SetResponse(false);
		}
			   
}

	public function obtain($id){
		$obtener = $this->db->from($this->tableDirection)
							->where('idDireccion', $id)
							->fetch();

				   $this->response->result = $obtener;
			return $this->response->SetResponse(true);
	}

	public function update($data, $id){

		$actualizar = $this->db->update($this->tableDirection, $data)
								->where('idDireccion', $id)
								->execute();

				   $this->response->result=$actualizar;
			return $this->response->SetResponse(true, 'Actualización correcta.');
	}
	
	public function delate($id){
		$delate = $this->db->update($this->tableDirection)
						   ->set('status', 0)
						   ->where('idDireccion',$id)
						   ->execute();

			   $this->response->result=$delate;
		return $this->response->SetResponse(true, 'Direccion Eliminada.');
	}

	public function cp($cp){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api-sepomex.hckdrk.mx/query/info_cp/$cp");
        // curl_setopt($ch, CURLOPT_HTTPHEADER, [
        //     'Authorization'. ':' .'Basic '. $this->authFacturama
        // ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $res = curl_exec($ch);
        curl_close($ch);
        $d = json_decode($res);
        $arrayColonias = [];
        if (isset($d->error)) {
                   $this->response->errors = $d->error_message;
            return $this->response->SetResponse(false);
        }else{
            $ciudad = $d[0]->response->ciudad;
            $municipio = $d[0]->response->municipio;
            $estado = $d[0]->response->estado;
            
            foreach ($d as $elemento) {
                $arrayColonias[] = $elemento->response->asentamiento;
            }
            $output = [
                "ciudad" => $ciudad,
                "municipio" => $municipio,
                "estado" =>$estado,
                "colonias" => $arrayColonias
            ];
            
                    $this->response->result = $output;
            return $this->response->SetResponse(true);
        }
    }

}
<?php 
use App\Lib\Response,
    App\Middleware\AuthMiddleware;


 $app->group('/schedule/', function () {
   $this->get('listSchedule/{id}', function($req, $res, $args){
      return $res->withHeader('Content-type', 'application/json')
                  ->write(
                  json_encode($this->model->schedule->listSchedule($args['id']))
               );
   });

   $this->post('add',function($req, $res, $args){
      $data = $req->getParsedBody();
   return $res->withHeader('Content-type', 'application/json')
            ->write(
            json_encode($this->model->schedule->add($data))
            );
   });

   $this->put('update/{id}',function($req, $res, $args){
   $data = $req->getParsedBody();
   return $res->withHeader('Content-type', 'application/json')
            ->write(
            json_encode($this->model->schedule->update($data,$args['id']))
            );
   });

 })->add(new AuthMiddleware($app));
 ?>
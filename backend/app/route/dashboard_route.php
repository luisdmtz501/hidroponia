<?php 
use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/dashboard/', function(){
    $this->get('reportDashboard/{de}/{a}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->dashboard->reportDashboard($arg['de'],$arg['a']))
				   );
    });
	
})->add(new AuthMiddleware($app));
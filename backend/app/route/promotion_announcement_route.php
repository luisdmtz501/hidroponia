<?php 
use App\Lib\Auth,
	App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/promotion_announcement/', function(){
	
	$this->post('add', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->promotion_announcement->add($req->getParsedBody()))
				   );
	});
	
	$this->put('update/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					  json_encode($this->model->promotion_announcement->update($req->getParsedBody(), $args['id']))
				   );
	});

    $this->get('detail/{id}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
						json_encode($this->model->promotion_announcement->detail($args['id']))
				   );
	});

	$this->get('listTipo', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->promotion_announcement->listTipo($req->getParsedBody()))
					);
  	});
	
	$this->delete('delete/{idPromociones_Anuncios}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					  json_encode($this->model->promotion_announcement->delete($args['idPromociones_Anuncios']))
					);            
	});

	$this->get('list', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->promotion_announcement->list($args))
					);
	});
	
	$this->get('listBoletos/{id}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
					->write(
						json_encode($this->model->promotion_announcement->listarBoletos($args['id']))
					);
  	});


})->add(new AuthMiddleware($app));
<?php
namespace App\Validation;
use App\Lib\Response;

class placeValidation {
    public static function validate($data, $update = false) {
        $response = new Response();

        $key = 'nombre';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Nombre es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 3) {
                $response->errors[$key] = 'Nombre no vallido.';
            }
        }

        $key = 'direccion';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Los apellidos son obligatorios';
        } else {
            $value = $data[$key];
        }

        $key = 'correo';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!filter_var($value,FILTER_VALIDATE_EMAIL)) {
                $response->errors[$key] = 'Valor ingresado no es un correo';
            }
        }

        $key = 'telefono';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Número obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) != 10) {
                $response->errors[$key] = 'Debe contener 10 caracteres.';
            }
        }


        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>
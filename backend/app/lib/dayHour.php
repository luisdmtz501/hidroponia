<?php
namespace App\Lib;

class DayHour
{
	
	public static function get()
	{
		$hora = date("'H:i:s'");
        $diaSis = date("l");
        $dia = 0;
        switch ($diaSis) {
            case 'Sunday':
                $dia = 7;
                break;
            case 'Monday':
                $dia = 1;
                break;
            case 'Tuesday':
                $dia = 2;
                break;
            case 'Wednesday':
                $dia = 3;
                break;
            case 'Thursday':
                $dia = 4;
                break;
            case 'Friday':
                $dia = 5;
                break;
            case 'Saturday':
                $dia = 6;
                break;
        }
		return ["dia"=>$dia,"hora"=>$hora];
	}
}
?>	
<?php
namespace App\Lib;

class Configuracion{

    private static $tipoServidor = 'local';
    // private static $tipoServidor = 'dev';
    // private static $tipoServidor = 'prod';
    
    private static $url = [
        'local'=>[
            'servidor' => '/var/www/html/hidroponia/backend/',
            'host'     => 'http://localhost/hidroponia/backend/'
        ],
        'dev'=> [
            'servidor'  => '/home//',
            'host'      => 'https://'
        ],
        'prod'=>[
            'servidor'  => '/home//',
            'host'      => 'https://'
        ]
    ];

    private static $twllio = [
        'local'=>[
            'sid'   => 'ACc3b2b2d61780a7d8427c2643765c56bd',
            'token' => 'd67ade821d2e57b60316d00f0eb132ae',
            'from'  => '+12565889648'
        ],
        'dev'=> [
            'sid'   => 'ACc3b2b2d61780a7d8427c2643765c56bd',
            'token' => 'd67ade821d2e57b60316d00f0eb132ae',
            'from'  => '+12565889648'
        ],
        'prod' => [
            'sid'   => 'ACbd0719bb3f4ba2d71eee5ee24d3c2915',
            'token' => 'b93a83b7098b21f6b4c317a150163d29',
            'from'  => '+14065100959'
        ]    
    ];
    
    private static $firebase = [
        'local'=>[
            'key'       =>  'AAAAajqZNgw:APA91bHx6ya0cIKN_KcTkJCqIwNO1iKKh7uVTkQ4tXuwJ4rfutN-xGfiWcXoglnZ2Tw4wjJ40wamn7QniVqh4dKEaFM9H1lq2Rk_jpQcUyDukJbSQv2gD-5aBkZFacH3h7JiKG-6e_K2',
            'urlRTDB'   =>  'https://??-staging.firebaseio.com/'
        ],
        'dev'=> [
            'key'       =>  'AAAAajqZNgw:APA91bHx6ya0cIKN_KcTkJCqIwNO1iKKh7uVTkQ4tXuwJ4rfutN-xGfiWcXoglnZ2Tw4wjJ40wamn7QniVqh4dKEaFM9H1lq2Rk_jpQcUyDukJbSQv2gD-5aBkZFacH3h7JiKG-6e_K2',
            'urlRTDB'   =>  'https://??-staging.firebaseio.com/'
        ],
        'prod'=>[
            'key'       =>  'AAAAf7UwhV0:APA91bFByBwVTryzwJ3gcYxdrLgISb04a5VpC2itMNabSB7C0fT6vxMoJHSF2a19w0cRL0y3zxqUKLnh6eWCp-b9FPrdJ-U5P657glt8BSh65tETnY7sC2N1Cfz6q3OtOrWI-yLUMeMw',
            'urlRTDB'   =>  'https://??.firebaseio.com/'
        ] 
    ];

    private static $urlRecover = [
        'local'=>[
            'url'       => "http://localhost/hidroponia/web/recover-passsword/authenticated-code.html"
        ],
        'dev'=> [
            'url'       => "https://??.com.mx/go/dev/web/recover-passsword/authenticated-code.html"
        ],
        'prod'=>[
            'url'       => "https://??.com.mx/go/web/recover-passsword/authenticated-code.html"
        ] 
    ];

    private static $conekta = [
        'local'=>[
            'key'=>"key_yawNuPVcAsFs2bfJsCpVCQ"#dev
        ],
        'dev'=> [
            'key'=>"key_yawNuPVcAsFs2bfJsCpVCQ"#dev
        ],
        'prod'=>[
            'key'=>"key_bFgzHopChrHtt4u4rf7uqg"#prod
        ]
    ];

    private static $facturama = [

    ];
    // funciones get
    public static function getUrl(){
        return self::$url[self::$tipoServidor];
    }
    public static function getTwilio(){
        return self::$twllio[self::$tipoServidor];
    }
    public static function getFirebase(){
        return self::$firebase[self::$tipoServidor];
    }
    public static function getUrlRecover(){
        return self::$urlRecover[self::$tipoServidor];
    }
}
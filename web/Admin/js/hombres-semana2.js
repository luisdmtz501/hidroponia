import {auth, hombresSemanaMes2, totalMeshombres} from './app.js';
import {cargarGeneric} from './generic/generics.js';

auth();
cargarGeneric();
totalMeshombres();
hombresSemanaMes2(1);


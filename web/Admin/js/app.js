import Api from '../../js/api/api.js'
import {url} from '../../js/config/config.js';

export const login = (usuario,password) =>{
    
    let servicio = `${url}auth/loginAdministrador`;
    let parametros = {
        "user": usuario,
        "password": password,
        "plataforma": "web"
    };
    let api = new Api(servicio,"POST",parametros);
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            sessionStorage.setItem('tokenAdmin',resultado.result.token);
            self.location = "menu-principal.html"
        }else{
           let modal = document.querySelector("#logoutModal");
           modal.style.display = "block";
           modal.classList.add('show');
        //    cuerpo = document.querySelector("#page-top");
        //    cuerpo.classList.add("modal-open");
           document.querySelector("#backgrondModal").style.display = "block";
        }
    })

    botonLogin.disabled = false;
    spinnerLogin.style.display = "none";
}
export const quitarLoader = () =>{
    // setTimeout(()=>document.getElementById('loaderPage').style.display = 'none', 1000);
    document.getElementById('loaderPage').style.display = 'none';
}
export const auth = () => {
    let token = sessionStorage.getItem('tokenAdmin');
    let servicio = `${url}auth/getDato/${token}`
    let api = new Api(servicio,"GET");
    let res = api.call();
    res.then(resultado => {
        if (resultado.response === false) {
            self.location = "index.html"
        }else{
            quitarLoader();
            console.log(resultado);
            let nombreAdmin = `${resultado.result.Nombre} ${resultado.result.Apellidos}`;
            document.querySelector(`#nombreAdmin`).innerHTML = nombreAdmin;
            let nombreAdminmenu = `${resultado.result.Nombre} ${resultado.result.Apellidos}`;
            document.querySelector(`#nombreAdminmenu`).innerHTML = nombreAdminmenu;
        }
    });
} 

export const logout = () => {
    sessionStorage.removeItem("tokenAdmin");
    self.location = "index.html"

}
export const llenarModalLoguot = () => {
    document.querySelector("#modalLabelTitle").innerHTML = "Cerrar sesion";
    document.querySelector("#modalLabelBody").innerHTML = "Esta seguro de cerrar su sesion?";
    document.querySelector("#modalActionButton").innerHTML = "Aceptar";
    document.querySelector("#modalActionButton").addEventListener("click", (e)=>{
        logout();
    })  
}
export const cerraModal = () => {
    let modal = document.querySelector("#logoutModal");
    modal.style.display = "none";
    modal.classList.remove('show');
    cuerpo = document.querySelector("#page-top");
    cuerpo.classList.remove("modal-open");
    document.querySelector("#backgrondModal").style.display = "none";
        
}


/**********************************************************************************************/
/**************************MUJERES*************************************************************/

  export const mujeresHoy = () => {
    let fechaIni  = document.querySelector("#FechaInicio").value;
    let fechaFin = document.querySelector("#FechaFin").value;
  //  let statusOrden = document.querySelector("#statusOrden").value;
    
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteHoy/${fechaIni}/${fechaFin}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            // window.detalleOrden = detalleOrden;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            // $('#dataTable').DataTable();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
}

//MUJERES SEMANAL 1
export const mujeresSemanal1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


  //MUJERES SEMANAL 1 DIA 1
  export const mujeresSemanal1Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 1 DIA 2
export const mujeresSemanal1Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 1 DIA 3
export const mujeresSemanal1Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 1 DIA 4
export const mujeresSemanal1Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 1 DIA 5
export const mujeresSemanal1Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 1 DIA 6
export const mujeresSemanal1Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 1 DIA 7
export const mujeresSemanal1Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}


//MUJERES SEMANAL 2
export const mujeresSemanal2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `Dia ${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`

            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 2 DIA 1
  export const mujeresSemanal2Dia1 = (tipo = null) => {
      if (tipo = 1){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //MUJERES SEMANAL 2 DIA 2
  export const mujeresSemanal2Dia2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //MUJERES SEMANAL 2 DIA 3
  export const mujeresSemanal2Dia3 = (tipo = null) => {
    if (tipo = 3){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MUJERES SEMANAL 2 DIA 4
  export const mujeresSemanal2Dia4 = (tipo = null) => {
    if (tipo = 4){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MUJERES SEMANAL 2 DIA 5
  export const mujeresSemanal2Dia5 = (tipo = null) => {
    if (tipo = 5){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MUJERES SEMANAL 2 DIA 6
  export const mujeresSemanal2Dia6 = (tipo = null) => {
    if (tipo = 6){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MUJERES SEMANAL 2 DIA 7
  export const mujeresSemanal2Dia7 = (tipo = null) => {
    if (tipo = 7){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MUJERES SEMANAL 2
  export const mujeresSemanal3 = (tipo = null) => {
      if (tipo = 3){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 3 DIA 1
  export const mujeresSemanal3Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 3 DIA 2
export const mujeresSemanal3Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 3 DIA 3
export const mujeresSemanal3Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 3 DIA 4
export const mujeresSemanal3Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 2 DIA 5
export const mujeresSemanal3Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 3 DIA 6
export const mujeresSemanal3Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 3 DIA 7
export const mujeresSemanal3Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MUJERES SEMANAL 4 
  export const mujeresSemanal4 = (tipo = null) => {
      if (tipo = 4){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 4 DIA 1
  export const mujeresSemanal4Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 4 DIA 2
export const mujeresSemanal4Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 4 DIA 3
export const mujeresSemanal4Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 4
export const mujeresSemanal4Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 5
export const mujeresSemanal4Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 6
export const mujeresSemanal4Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 7
export const mujeresSemanal4Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MUJERES SEMANAL 5
  export const mujeresSemanal5 = (tipo = null) => {
      if (tipo = 5){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 4 DIA 1
  export const mujeresSemanal5Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 4 DIA 2
export const mujeresSemanal5Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 4 DIA 3
export const mujeresSemanal5Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 4
export const mujeresSemanal5Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 5
export const mujeresSemanal5Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 6
export const mujeresSemanal5Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 4 DIA 7
export const mujeresSemanal5Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MUJERES SEMANAL 6
  export const mujeresSemanal6 = (tipo = null) => {
      if (tipo = 6){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 6 DIA 1
  export const mujeresSemanal6Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 6 DIA 2
export const mujeresSemanal6Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 6 DIA 3
export const mujeresSemanal6Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 6 DIA 4
export const mujeresSemanal6Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 6 DIA 5
export const mujeresSemanal6Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 6 DIA 6
export const mujeresSemanal6Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 7 DIA 7
export const mujeresSemanal6Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MUJERES SEMANAL 7
  export const mujeresSemanal7 = (tipo = null) => {
      if (tipo = 7){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
             let nombreAdmin = `${element.DIA_SEMANA}`;
             document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MUJERES SEMANAL 7 DIA 1
  export const mujeresSemanal7Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 7 DIA 2
export const mujeresSemanal7Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MUJERES SEMANAL 7 DIA 3
export const mujeresSemanal7Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 7 DIA 4
export const mujeresSemanal7Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 7 DIA 5
export const mujeresSemanal7Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 7 DIA 6
export const mujeresSemanal7Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES SEMANAL 7 DIA 7
export const mujeresSemanal7Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanal/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MUJERES TOTAL SEMANAL 
export const totalSemanal = (tipo = null) => {
      if (tipo = 8){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanal/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#TotalSemanal").innerHTML = `${Total}`;
              document.getElementById("TotalSemanal").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("TotalSemanal").innerHTML = `0`;
          }
      })
    }
  }

//MES MUJERES SEMANA 1 
export const mujeresSemanaMes1 = (tipo = null) => {
    if (tipo = 9){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES MUJERES SEMANA 2
export const mujeresSemanaMes2 = (tipo = null) => {
    if (tipo = 10){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


//MES MUJERES SEMANA 3
export const mujeresSemanaMes3 = (tipo = null) => {
    if (tipo = 11){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES MUJERES SEMANA 4
export const mujeresSemanaMes4 = (tipo = null) => {
    if (tipo = 12){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MUJERES TOTAL MES 
export const totalMes = (tipo = null) => {
    if (tipo = 13){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanal/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            body.innerHTML = ``;
            document.querySelector("#TotalMes").innerHTML = `${Total}`;
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

/**##################################################################################################**/
/**##################################################################################################**/
/**************************ADULTOS MAYORES*************************************************************/

export const adultosHoy = () => {
    let fechaIni  = document.querySelector("#FechaInicio").value;
    let fechaFin = document.querySelector("#FechaFin").value;
  //  let statusOrden = document.querySelector("#statusOrden").value;
    
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteHoyAdultos/${fechaIni}/${fechaFin}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            // window.detalleOrden = detalleOrden;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            // $('#dataTable').DataTable();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
}

//ADULTOS SEMANAL 2
export const adultosSemanal1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


  //ADULTOS SEMANAL 1 DIA 1
  export const adultosSemanal1Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 1 DIA 2
export const adultosSemanal1Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 1 DIA 3
export const adultosSemanal1Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 1 DIA 4
export const adultosSemanal1Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 1 DIA 5
export const adultosSemanal1Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 1 DIA 6
export const adultosSemanal1Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 1 DIA 7
export const adultosSemanal1Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}


//ADULTOS SEMANAL 2
export const adultosSemanal2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `Dia ${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`

            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 2 DIA 1
  export const adultosSemanal2Dia1 = (tipo = null) => {
      if (tipo = 1){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //ADULTOS SEMANAL 2 DIA 2
  export const adultosSemanal2Dia2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //ADULTOS SEMANAL 2 DIA 3
  export const adultosSemanal2Dia3 = (tipo = null) => {
    if (tipo = 3){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //ADULTOS SEMANAL 2 DIA 4
  export const adultosSemanal2Dia4 = (tipo = null) => {
    if (tipo = 4){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //ADULTOS SEMANAL 2 DIA 5
  export const adultosSemanal2Dia5 = (tipo = null) => {
    if (tipo = 5){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //ADULTOS SEMANAL 2 DIA 6
  export const adultosSemanal2Dia6 = (tipo = null) => {
    if (tipo = 6){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //ADULTOS SEMANAL 2 DIA 7
  export const adultosSemanal2Dia7 = (tipo = null) => {
    if (tipo = 7){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //ADULTOS SEMANAL 2
  export const adultosSemanal3 = (tipo = null) => {
      if (tipo = 3){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 3 DIA 1
  export const adultosSemanal3Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 3 DIA 2
export const adultosSemanal3Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 3 DIA 3
export const adultosSemanal3Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 3 DIA 4
export const adultosSemanal3Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 2 DIA 5
export const adultosSemanal3Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 3 DIA 6
export const adultosSemanal3Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 3 DIA 7
export const adultosSemanal3Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //ADULTOS SEMANAL 4 
  export const adultosSemanal4 = (tipo = null) => {
      if (tipo = 4){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 4 DIA 1
  export const adultosSemanal4Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 4 DIA 2
export const adultosSemanal4Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 4 DIA 3
export const adultosSemanal4Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 4
export const adultosSemanal4Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 5
export const adultosSemanal4Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 6
export const adultosSemanal4Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 7
export const adultosSemanal4Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //ADULTOS SEMANAL 5
  export const adultosSemanal5 = (tipo = null) => {
      if (tipo = 5){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 4 DIA 1
  export const adultosSemanal5Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 4 DIA 2
export const adultosSemanal5Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 4 DIA 3
export const adultosSemanal5Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 4
export const adultosSemanal5Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 5
export const adultosSemanal5Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 6
export const adultosSemanal5Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 4 DIA 7
export const adultosSemanal5Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //ADULTOS SEMANAL 6
  export const adultosSemanal6 = (tipo = null) => {
      if (tipo = 6){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 6 DIA 1
  export const adultosSemanal6Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 6 DIA 2
export const adultosSemanal6Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 6 DIA 3
export const adultosSemanal6Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 6 DIA 4
export const adultosSemanal6Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 6 DIA 5
export const adultosSemanal6Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 6 DIA 6
export const adultosSemanal6Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 7 DIA 7
export const adultosSemanal6Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //ADULTOS SEMANAL 7
  export const adultosSemanal7 = (tipo = null) => {
      if (tipo = 7){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
             let nombreAdmin = `${element.DIA_SEMANA}`;
             document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //ADULTOS SEMANAL 7 DIA 1
  export const adultosSemanal7Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 7 DIA 2
export const adultosSemanal7Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//ADULTOS SEMANAL 7 DIA 3
export const adultosSemanal7Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 7 DIA 4
export const adultosSemanal7Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 7 DIA 5
export const adultosSemanal7Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 7 DIA 6
export const adultosSemanal7Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS SEMANAL 7 DIA 7
export const adultosSemanal7Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//ADULTOS TOTAL SEMANAL 
export const totalSemanaladultos = (tipo = null) => {
      if (tipo = 8){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#TotalSemanal").innerHTML = `${Total}`;
              document.getElementById("TotalSemanal").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("TotalSemanal").innerHTML = `0`;
          }
      })
    }
  }

  
//MES ADULTOS SEMANA 1 
export const adultosSemanaMes1 = (tipo = null) => {
    if (tipo = 9){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES ADULTOS SEMANA 2
export const adultosSemanaMes2 = (tipo = null) => {
    if (tipo = 10){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


//MES ADULTOS SEMANA 3
export const adultosSemanaMes3 = (tipo = null) => {
    if (tipo = 11){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES ADULTOS SEMANA 4
export const adultosSemanaMes4 = (tipo = null) => {
    if (tipo = 12){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//ADULTOS TOTAL MES 
export const totalMesadultos = (tipo = null) => {
    if (tipo = 13){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalAdultos/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            body.innerHTML = ``;
            document.querySelector("#TotalMes").innerHTML = `${Total}`;
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}


/**##################################################################################################**/
/**##################################################################################################**/
/**************************MENORES DE EDAD*************************************************************/

export const menoresHoy = () => {
    let fechaIni  = document.querySelector("#FechaInicio").value;
    let fechaFin = document.querySelector("#FechaFin").value;
  //  let statusOrden = document.querySelector("#statusOrden").value;
    
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteHoyMenores/${fechaIni}/${fechaFin}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            // window.detalleOrden = detalleOrden;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            // $('#dataTable').DataTable();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
}

//MENORES SEMANAL 2
export const menoresSemanal1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


  //MENORES SEMANAL 1 DIA 1
  export const menoresSemanal1Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 1 DIA 2
export const menoresSemanal1Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 1 DIA 3
export const menoresSemanal1Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 4
export const menoresSemanal1Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 5
export const menoresSemanal1Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 6
export const menoresSemanal1Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 7
export const menoresSemanal1Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}


//MENORES SEMANAL 2
export const menoresSemanal2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `Dia ${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`

            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 2 DIA 1
  export const menoresSemanal2Dia1 = (tipo = null) => {
      if (tipo = 1){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //MENORES SEMANAL 2 DIA 2
  export const menoresSemanal2Dia2 = (tipo = null) => {
      if (tipo = 2){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
              Alertas.forEach(element => {
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
              });
              quitarLoader();
          }else{
          }
      })
    }
  }

  //MENORES SEMANAL 2 DIA 3
  export const menoresSemanal2Dia3 = (tipo = null) => {
    if (tipo = 3){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MENORES SEMANAL 2 DIA 4
  export const menoresSemanal2Dia4 = (tipo = null) => {
    if (tipo = 4){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MENORES SEMANAL 2 DIA 5
  export const menoresSemanal2Dia5 = (tipo = null) => {
    if (tipo = 5){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MENORES SEMANAL 2 DIA 6
  export const menoresSemanal2Dia6 = (tipo = null) => {
    if (tipo = 6){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MENORES SEMANAL 2 DIA 7
  export const menoresSemanal2Dia7 = (tipo = null) => {
    if (tipo = 7){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

  //MENORES SEMANAL 2
  export const menoresSemanal3 = (tipo = null) => {
      if (tipo = 3){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 3 DIA 1
  export const menoresSemanal3Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 3 DIA 2
export const menoresSemanal3Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 3 DIA 3
export const menoresSemanal3Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 3 DIA 4
export const menoresSemanal3Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2 DIA 5
export const menoresSemanal3Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 3 DIA 6
export const menoresSemanal3Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 3 DIA 7
export const menoresSemanal3Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MENORES SEMANAL 4 
  export const menoresSemanal4 = (tipo = null) => {
      if (tipo = 4){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 4 DIA 1
  export const menoresSemanal4Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 2
export const menoresSemanal4Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 3
export const menoresSemanal4Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 4
export const menoresSemanal4Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 5
export const menoresSemanal4Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 6
export const menoresSemanal4Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 7
export const menoresSemanal4Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MENORES SEMANAL 5
  export const menoresSemanal5 = (tipo = null) => {
      if (tipo = 5){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 4 DIA 1
  export const menoresSemanal5Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 2
export const menoresSemanal5Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 3
export const menoresSemanal5Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 4
export const menoresSemanal5Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 5
export const menoresSemanal5Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 6
export const menoresSemanal5Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 7
export const menoresSemanal5Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MENORES SEMANAL 6
  export const menoresSemanal6 = (tipo = null) => {
      if (tipo = 6){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
            let nombreAdmin = `${element.DIA_SEMANA}`;
            document.querySelector(`#Dia`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 6 DIA 1
  export const menoresSemanal6Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 6 DIA 2
export const menoresSemanal6Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 6 DIA 3
export const menoresSemanal6Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 6 DIA 4
export const menoresSemanal6Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 6 DIA 5
export const menoresSemanal6Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 6 DIA 6
export const menoresSemanal6Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 7
export const menoresSemanal6Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

  //MENORES SEMANAL 7
  export const menoresSemanal7 = (tipo = null) => {
      if (tipo = 7){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#Total").innerHTML = `${Total}`;
              Alertas.forEach(element => {
                  let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                      <td>${element.FechaEmision}</td>
                                      <td>${element.HoraEmision}</td>
                                      <td>${element.Descripcion}</td>
                                  </tr> `;
                                  body.innerHTML += `${innerHTML}`
             let nombreAdmin = `${element.DIA_SEMANA}`;
             document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
              });
              document.getElementById("Total").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("Total").innerHTML = `0`;
          }
      })
    }
  }

  //MENORES SEMANAL 7 DIA 1
  export const menoresSemanal7Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 7 DIA 2
export const menoresSemanal7Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 7 DIA 3
export const menoresSemanal7Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 4
export const menoresSemanal7Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 5
export const menoresSemanal7Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 6
export const menoresSemanal7Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 7
export const menoresSemanal7Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES TOTAL SEMANAL 
export const totalSemanalmenores = (tipo = null) => {
      if (tipo = 8){
      let body = document.querySelector("#bodyTable");
      let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
      let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
      let res = api.call();
      res.then(resultado => {
          if (resultado.response) {
              let Alertas = resultado.result.Alertas;
              console.log(Alertas);
              let Total = resultado.result.Total;
              let Dia = resultado.result.Dia;
              body.innerHTML = ``;
              document.querySelector("#TotalSemanal").innerHTML = `${Total}`;
              document.getElementById("TotalSemanal").innerHTML = Alertas.length;
              quitarLoader();
          }else{
              body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
              document.getElementById("TotalSemanal").innerHTML = `0`;
          }
      })
    }
  }

  
//MES MENORES SEMANA 1 
export const menoresSemanaMes1 = (tipo = null) => {
    if (tipo = 9){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES MENORES SEMANA 2
export const menoresSemanaMes2 = (tipo = null) => {
    if (tipo = 10){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}


//MES MENORES SEMANA 3
export const menoresSemanaMes3 = (tipo = null) => {
    if (tipo = 11){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MES MENORES SEMANA 4
export const menoresSemanaMes4 = (tipo = null) => {
    if (tipo = 12){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.DIA_SEMANA}</td>
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
         //   let nombreAdmin = `${element.DIA_SEMANA}`;
         //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
            
        }
    })
  }
}

//MENORES TOTAL MES 
export const totalMesmenores = (tipo = null) => {
    if (tipo = 13){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            body.innerHTML = ``;
            document.querySelector("#TotalMes").innerHTML = `${Total}`;
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

/**######################################################################################**/
/**##################################################################################################**/
/**##################################################################################################**/
/**************************HOMBRES*******************************************************************/

export const hombresHoy = () => {
  let fechaIni  = document.querySelector("#FechaInicio").value;
  let fechaFin = document.querySelector("#FechaFin").value;
//  let statusOrden = document.querySelector("#statusOrden").value;
  
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteHoyHombres/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          // window.detalleOrden = detalleOrden;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          // $('#dataTable').DataTable();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}

//MENORES SEMANAL 2
export const hombresSemanal1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          let Dia = resultado.result.Dia;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Dia ${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
          
      }
  })
}
}


//MENORES SEMANAL 1 DIA 1
export const hombresSemanal1Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 2
export const hombresSemanal1Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 1 DIA 3
export const hombresSemanal1Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 1 DIA 4
export const hombresSemanal1Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 1 DIA 5
export const hombresSemanal1Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalMenores/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 1 DIA 6
export const hombresSemanal1Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 1 DIA 7
export const hombresSemanal1Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}


//MENORES SEMANAL 2
export const hombresSemanal2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `Dia ${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`

          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 2 DIA 1
export const hombresSemanal2Dia1 = (tipo = null) => {
    if (tipo = 1){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 2 DIA 2
export const hombresSemanal2Dia2 = (tipo = null) => {
    if (tipo = 2){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
            Alertas.forEach(element => {
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
            });
            quitarLoader();
        }else{
        }
    })
  }
}

//MENORES SEMANAL 2 DIA 3
export const hombresSemanal2Dia3 = (tipo = null) => {
  if (tipo = 3){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2 DIA 4
export const hombresSemanal2Dia4 = (tipo = null) => {
  if (tipo = 4){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2 DIA 5
export const hombresSemanal2Dia5 = (tipo = null) => {
  if (tipo = 5){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2 DIA 6
export const hombresSemanal2Dia6 = (tipo = null) => {
  if (tipo = 6){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2 DIA 7
export const hombresSemanal2Dia7 = (tipo = null) => {
  if (tipo = 7){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 2
export const hombresSemanal3 = (tipo = null) => {
    if (tipo = 3){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 3 DIA 1
export const hombresSemanal3Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 3 DIA 2
export const hombresSemanal3Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 3 DIA 3
export const hombresSemanal3Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 3 DIA 4
export const hombresSemanal3Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 2 DIA 5
export const hombresSemanal3Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 3 DIA 6
export const hombresSemanal3Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 3 DIA 7
export const hombresSemanal3Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 
export const hombresSemanal4 = (tipo = null) => {
    if (tipo = 4){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 1
export const hombresSemanal4Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 2
export const hombresSemanal4Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 3
export const hombresSemanal4Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 4
export const hombresSemanal4Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 5
export const hombresSemanal4Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 6
export const hombresSemanal4Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 7
export const hombresSemanal4Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 5
export const hombresSemanal5 = (tipo = null) => {
    if (tipo = 5){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 4 DIA 1
export const hombresSemanal5Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 2
export const hombresSemanal5Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 4 DIA 3
export const hombresSemanal5Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 4
export const hombresSemanal5Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 5
export const hombresSemanal5Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 6
export const hombresSemanal5Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 4 DIA 7
export const hombresSemanal5Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 6
export const hombresSemanal6 = (tipo = null) => {
    if (tipo = 6){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
          let nombreAdmin = `${element.DIA_SEMANA}`;
          document.querySelector(`#Dia`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 6 DIA 1
export const hombresSemanal6Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 6 DIA 2
export const hombresSemanal6Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 6 DIA 3
export const hombresSemanal6Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 6 DIA 4
export const hombresSemanal6Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 6 DIA 5
export const hombresSemanal6Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 6 DIA 6
export const hombresSemanal6Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7 DIA 7
export const hombresSemanal6Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7
export const hombresSemanal7 = (tipo = null) => {
    if (tipo = 7){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#Total").innerHTML = `${Total}`;
            Alertas.forEach(element => {
                let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                    <td>${element.FechaEmision}</td>
                                    <td>${element.HoraEmision}</td>
                                    <td>${element.Descripcion}</td>
                                </tr> `;
                                body.innerHTML += `${innerHTML}`
           let nombreAdmin = `${element.DIA_SEMANA}`;
           document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
            });
            document.getElementById("Total").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("Total").innerHTML = `0`;
        }
    })
  }
}

//MENORES SEMANAL 7 DIA 1
export const hombresSemanal7Dia1 = (tipo = null) => {
  if (tipo = 1){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 2
export const hombresSemanal7Dia2 = (tipo = null) => {
  if (tipo = 2){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
          Alertas.forEach(element => {
        let nombreAdmin = `${element.DIA_SEMANA}`;
        document.querySelector(`#Dia2`).innerHTML = nombreAdmin;
          });
          quitarLoader();
      }else{
      }
  })
}
}

//MENORES SEMANAL 7 DIA 3
export const hombresSemanal7Dia3 = (tipo = null) => {
if (tipo = 3){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia3`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7 DIA 4
export const hombresSemanal7Dia4 = (tipo = null) => {
if (tipo = 4){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia4`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7 DIA 5
export const hombresSemanal7Dia5 = (tipo = null) => {
if (tipo = 5){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia5`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7 DIA 6
export const hombresSemanal7Dia6 = (tipo = null) => {
if (tipo = 6){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia6`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES SEMANAL 7 DIA 7
export const hombresSemanal7Dia7 = (tipo = null) => {
if (tipo = 7){
let body = document.querySelector("#bodyTable");
let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
let res = api.call();
res.then(resultado => {
    if (resultado.response) {
        let Alertas = resultado.result.Alertas;
        console.log(Alertas);// document.querySelector("#Dia").innerHTML = `Dia ${Dia}`;
        Alertas.forEach(element => {
      let nombreAdmin = `${element.DIA_SEMANA}`;
      document.querySelector(`#Dia7`).innerHTML = nombreAdmin;
        });
        quitarLoader();
    }else{
    }
})
}
}

//MENORES TOTAL SEMANAL 
export const totalSemanalhombres = (tipo = null) => {
    if (tipo = 8){
    let body = document.querySelector("#bodyTable");
    let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let Alertas = resultado.result.Alertas;
            console.log(Alertas);
            let Total = resultado.result.Total;
            let Dia = resultado.result.Dia;
            body.innerHTML = ``;
            document.querySelector("#TotalSemanal").innerHTML = `${Total}`;
            document.getElementById("TotalSemanal").innerHTML = Alertas.length;
            quitarLoader();
        }else{
            body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
            document.getElementById("TotalSemanal").innerHTML = `0`;
        }
    })
  }
}


//MES MENORES SEMANA 1 
export const hombresSemanaMes1 = (tipo = null) => {
  if (tipo = 9){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          let Dia = resultado.result.Dia;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Dia ${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.DIA_SEMANA}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
       //   let nombreAdmin = `${element.DIA_SEMANA}`;
       //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
          
      }
  })
}
}

//MES MENORES SEMANA 2
export const hombresSemanaMes2 = (tipo = null) => {
  if (tipo = 10){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          let Dia = resultado.result.Dia;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Dia ${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.DIA_SEMANA}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
       //   let nombreAdmin = `${element.DIA_SEMANA}`;
       //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
          
      }
  })
}
}


//MES MENORES SEMANA 3
export const hombresSemanaMes3 = (tipo = null) => {
  if (tipo = 11){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          let Dia = resultado.result.Dia;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Dia ${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.DIA_SEMANA}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
       //   let nombreAdmin = `${element.DIA_SEMANA}`;
       //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
          
      }
  })
}
}

//MES MENORES SEMANA 4
export const hombresSemanaMes4 = (tipo = null) => {
  if (tipo = 12){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          let Dia = resultado.result.Dia;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Dia ${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.DIA_SEMANA}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
       //   let nombreAdmin = `${element.DIA_SEMANA}`;
       //   document.querySelector(`#Dia1`).innerHTML = nombreAdmin;
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
          
      }
  })
}
}

//MENORES TOTAL MES 
export const totalMeshombres = (tipo = null) => {
  if (tipo = 13){
  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalHombres/${tipo}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          body.innerHTML = ``;
          document.querySelector("#TotalMes").innerHTML = `${Total}`;
          document.getElementById("Total").innerHTML = Alertas.length;
          quitarLoader();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}
}

/**######################################################################################**/
/**######################################################################################**/
/**##################################HUBICACION####################################################**/


//estado puebla
export const reporteEstado = () => {
  let fechaIni  = document.querySelector("#FechaInicio").value;
  let fechaFin = document.querySelector("#FechaFin").value;
//  let statusOrden = document.querySelector("#statusOrden").value;

  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalEstado/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          // window.detalleOrden = detalleOrden;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                                  <td>${element.Sexo}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          // $('#dataTable').DataTable();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}

//estado ciudad de mexico
export const reporteEstado2 = () => {
  let fechaIni  = document.querySelector("#FechaInicio").value;
  let fechaFin = document.querySelector("#FechaFin").value;
//  let statusOrden = document.querySelector("#statusOrden").value;

  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteSemanalEstado2/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          // window.detalleOrden = detalleOrden;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                                  <td>${element.Sexo}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          // $('#dataTable').DataTable();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}


//estado ciudad de mexico
export const reporteMunicipio = () => {
  let fechaIni  = document.querySelector("#FechaInicio").value;
  let fechaFin = document.querySelector("#FechaFin").value;
//  let statusOrden = document.querySelector("#statusOrden").value;

  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteMunicipio/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          // window.detalleOrden = detalleOrden;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.Nombre}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                                  <td>${element.Sexo}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          // $('#dataTable').DataTable();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}

//colonias
export const reporteColonias = () => {
  let fechaIni  = document.querySelector("#FechaInicio").value;
  let fechaFin = document.querySelector("#FechaFin").value;
//  let statusOrden = document.querySelector("#statusOrden").value;

  let body = document.querySelector("#bodyTable");
  let servicio = `${url}alerta/reporteColonias/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let Alertas = resultado.result.Alertas;
          console.log(Alertas);
          let Total = resultado.result.Total;
          // window.detalleOrden = detalleOrden;
          body.innerHTML = ``;
          document.querySelector("#Total").innerHTML = `Monto: $${Total}`;
          Alertas.forEach(element => {
              let innerHTML = ` <tr class="table table-bordered text-gray-700">
                                  <td>${element.Colonia}</td>
                                  <td>${element.FechaEmision}</td>
                                  <td>${element.HoraEmision}</td>
                                  <td>${element.Descripcion}</td>
                                  <td>${element.Sexo}</td>
                              </tr> `;
                              body.innerHTML += `${innerHTML}`
          });
          document.getElementById("Total").innerHTML = Alertas.length;
          // $('#dataTable').DataTable();
      }else{
          body.innerHTML = `<p style="color:red" >${resultado.errors}</p>`;
          document.getElementById("Total").innerHTML = `0`;
      }
  })
}



/**######################################################################################**/

//ReportDashboard 
export const bucarReporteDashboard = () => {
    let fechaIni  = document.querySelector("#FechaIn").value;
    let fechaFin = document.querySelector("#FechaFi").value;
    if (fechaIni == '' || fechaFin == '') {
        alert("Ingrese fechas de inicio y fin validas")
        return false;
    }
    let servicio = `${url}alerta/reportDashboard/${fechaIni}/${fechaFin}`;
    let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
    let res = api.call();
    res.then(resultado => {
        if (resultado.response) {
            let data = resultado.result.data;
            document.querySelector("#labelTotal").innerHTML = ` ${resultado.result.total}`;
            console.log(data);
            document.querySelector(`#Robo`).innerHTML = `${data.Robo}`;
            document.querySelector(`#Secuestro`).innerHTML = `${data.Secuestro}`;
            document.querySelector(`#Accidente`).innerHTML = `${data.Accidente}`;
            document.querySelector(`#ViolenciaFamiliar`).innerHTML = `${data.ViolenciaFamiliar}`;
            document.querySelector(`#ProblemaSalud`).innerHTML = `${data.ProblemaSalud}`;
    }
  });
}

//ReportDashboard General
export const bucarReporteDashboardG = () => {
  let fechaIni  = document.querySelector("#FechaIn").value;
  let fechaFin = document.querySelector("#FechaFi").value;
  if (fechaIni == '' || fechaFin == '') {
      alert("Ingrese fechas de inicio y fin validas")
      return false;
  }
  let servicio = `${url}alerta/reportDashboardG/${fechaIni}/${fechaFin}`;
  let api = new Api(servicio,"GET",null,sessionStorage.getItem('tokenAdmin'));
  let res = api.call();
  res.then(resultado => {
      if (resultado.response) {
          let data = resultado.result.data;
          document.querySelector("#labelTotalG").innerHTML = ` ${resultado.result.total}`;
          console.log(data);
  }
});
}



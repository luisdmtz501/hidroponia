let url = false;
let refUrlPag = false;
let conekta = false;
let configFireBase = false;

// const modoServidor = 'local';
 const modoServidor = 'dev';
// const modoServidor = 'prod';

if (modoServidor === 'local') {
    url = `http://localhost/alerta-ciudadana/security_app_back/public/`;
    refUrlPag = `http://localhost/alerta-ciudadana/web/`;
   // conekta = 'key_DY6cGoLsFLGawX9sowxgj2g';
   /* configFireBase = {
        apiKey: "AIzaSyCnPIhjBDhxehDXdgsSrMbgSfrBHXecrfU",
        authDomain: "alerta-staging.firebaseapp.com",
        databaseURL: "https://alerta-staging.firebaseio.com",
        projectId: "alerta-staging",
        storageBucket: "alerta-staging.appspot.com",
        messagingSenderId: "456249652748",
        appId: "1:456249652748:web:600d2361fc9bf4ecdde029",
        measurementId: "G-VFHBLVNBT1"
    }*/
} else if (modoServidor === 'dev') {
    url = `http://dev.alerta-ciudadana.com/security_app_back/public/`;
    refUrlPag = `http://dev.alerta-ciudadana.com/web/`;
   // conekta = 'key_DY6cGoLsFLGawX9sowxgj2g';
   /* configFireBase = {
        apiKey: "AIzaSyCnPIhjBDhxehDXdgsSrMbgSfrBHXecrfU",
        authDomain: "alerta-staging.firebaseapp.com",
        databaseURL: "https://alerta-staging.firebaseio.com",
        projectId: "alerta-staging",
        storageBucket: "alerta-staging.appspot.com",
        messagingSenderId: "456249652748",
        appId: "1:456249652748:web:600d2361fc9bf4ecdde029",
        measurementId: "G-VFHBLVNBT1"
    }*/
} else if (modoServidor === 'prod') {
    url = `https://dev.alerta-ciudadana.com/security_app_back/public/`;
    refUrlPag = `https://dev.alerta-ciudadana.com/web/`;
   // conekta = 'key_UjWDsad3eqPfAgWDuGRjpxg';
   /* configFireBase = {
        apiKey: "AIzaSyAD9rT4efip-_Ap9PWAJZlfmzgWVKeryZw",
        authDomain: "alerta.firebaseapp.com",
        databaseURL: "https://alerta.firebaseio.com",
        projectId: "alerta",
        storageBucket: "alerta.appspot.com",
        messagingSenderId: "548500702557",
        appId: "1:548500702557:web:fae8488b2b5463662d14b4",
        measurementId: "G-JMC0C08BWR"
      }*/
}

export {
    url,
    refUrlPag,
    conekta,
    configFireBase
}